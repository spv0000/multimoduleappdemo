package velichko.semyon.demo.navigator

import android.content.Context
import android.content.Intent
import velichko.semyon.demo.core.di.scopes.navigator.KEY_URL
import velichko.semyon.demo.core.di.scopes.navigator.Navigator
import velichko.semyon.demo.details.DetailsActivity

class NavigatorImpl(private val context: Context) : Navigator {

    override fun showDetailsScreen(url: String) {

        val intent = Intent(context, DetailsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(KEY_URL, url)
        context.startActivity(intent)
    }
}