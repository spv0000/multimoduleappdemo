package velichko.semyon.demo

import android.app.Application
import velichko.semyon.demo.core.di.scopes.api.RetrofitModule
import velichko.semyon.demo.core.di.scopes.di.AppComponent
import velichko.semyon.demo.core.di.scopes.di.AppComponentProvider
import velichko.semyon.demo.core.di.scopes.di.AppModule
import velichko.semyon.demo.core.di.scopes.di.DaggerAppComponent
import velichko.semyon.demo.core.di.scopes.router.di.RouterModule
import velichko.semyon.demo.navigator.NavigatorImpl

internal class App : Application() {

    override fun onCreate() {
        super.onCreate()

        AppComponentProvider.component = getAppComponent()
    }

    private fun getAppComponent(): AppComponent {

        val baseUrl = "https://api.flickr.com"

        return DaggerAppComponent
            .builder()
            .appModule(AppModule(applicationContext))
            .routerModule(RouterModule(NavigatorImpl(applicationContext)))
            .retrofitModule(RetrofitModule(baseUrl))
            .build()
    }
}