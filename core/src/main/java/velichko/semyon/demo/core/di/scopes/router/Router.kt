package velichko.semyon.demo.core.di.scopes.router

interface Router {

    fun showDetails(url: String)
}