package velichko.semyon.demo.core.di.scopes.observer

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class ConciseObserver<T>(

    private val onSuccessAction: (result: T) -> Unit,
    private val onErrorAction: () -> Unit

) : SingleObserver<T> {

    override fun onSubscribe(d: Disposable) {}

    override fun onSuccess(value: T) {
        onSuccessAction.invoke(value)
    }

    override fun onError(e: Throwable) {
        onErrorAction.invoke()
    }
}