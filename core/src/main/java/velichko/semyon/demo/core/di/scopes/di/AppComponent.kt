package velichko.semyon.demo.core.di.scopes.di

import android.content.Context
import dagger.Component
import retrofit2.Retrofit
import velichko.semyon.demo.core.di.scopes.ApplicationScope
import velichko.semyon.demo.core.di.scopes.info.ScreenInfo
import velichko.semyon.demo.core.di.scopes.router.Router

@ApplicationScope
@Component(
    modules = [AppModule::class]
)
interface AppComponent {

    val router: Router
    val screenInfo: ScreenInfo
    val retrofit: Retrofit
    val context: Context
}