package velichko.semyon.demo.core.di.scopes.di

import android.content.Context
import dagger.Module
import dagger.Provides
import velichko.semyon.demo.core.di.scopes.ApplicationScope
import velichko.semyon.demo.core.di.scopes.api.RetrofitModule
import velichko.semyon.demo.core.di.scopes.info.ScreenInfo
import velichko.semyon.demo.core.di.scopes.router.di.RouterModule

@ApplicationScope
@Module(
    includes = [RouterModule::class, RetrofitModule::class]
)
class AppModule(val context: Context) {

    @Provides
    fun providesContext() = context

    @Provides
    fun providesScreenInfo() = ScreenInfo(context)
}