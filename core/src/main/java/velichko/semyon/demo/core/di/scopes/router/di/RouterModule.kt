package velichko.semyon.demo.core.di.scopes.router.di

import dagger.Module
import dagger.Provides
import velichko.semyon.demo.core.di.scopes.ApplicationScope
import velichko.semyon.demo.core.di.scopes.navigator.Navigator
import velichko.semyon.demo.core.di.scopes.router.Router
import velichko.semyon.demo.core.di.scopes.router.RouterImpl

@ApplicationScope
@Module
class RouterModule(private val navigator: Navigator) {

    @Provides
    fun providesRouter(): Router = RouterImpl(navigator)
}