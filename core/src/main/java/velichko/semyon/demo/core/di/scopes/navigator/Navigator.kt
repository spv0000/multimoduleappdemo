package velichko.semyon.demo.core.di.scopes.navigator

const val KEY_URL = "url"

interface Navigator {

    fun showDetailsScreen(url: String)
}