package velichko.semyon.demo.core.di.scopes.api

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import velichko.semyon.demo.core.di.scopes.ApplicationScope
import java.util.concurrent.TimeUnit

private const val TIMEOUT = 10000L

@ApplicationScope
@Module
class RetrofitModule(private val baseUrl: String) {

    @Provides
    fun providesRetrofit(): Retrofit = getRetrofit()

    private fun getRetrofit(): Retrofit {

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private fun getOkHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level =  HttpLoggingInterceptor.Level.BODY})
            .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
            .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
            .build()
    }
}