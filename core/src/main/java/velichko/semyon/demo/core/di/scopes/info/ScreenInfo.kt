package velichko.semyon.demo.core.di.scopes.info

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.graphics.Point
import android.os.Build
import android.view.WindowManager

class ScreenInfo(

    private val context: Context

) {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    fun getScreenSize(): Point {
        val display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        val size = Point()
        display.getSize(size)
        return size
    }

    fun isVerticalOrientation() = context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
}