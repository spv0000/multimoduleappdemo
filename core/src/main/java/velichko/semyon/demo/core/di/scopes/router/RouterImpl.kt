package velichko.semyon.demo.core.di.scopes.router

import velichko.semyon.demo.core.di.scopes.navigator.Navigator

internal class RouterImpl(private val navigator: Navigator) : Router {

    override fun showDetails(url: String) {
        navigator.showDetailsScreen(url)
    }
}