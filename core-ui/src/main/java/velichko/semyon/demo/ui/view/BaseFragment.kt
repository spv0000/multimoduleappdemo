package velichko.semyon.demo.ui.view

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dagger.Lazy
import velichko.semyon.demo.ui.event.item.ViewEvent
import velichko.semyon.demo.ui.event.item.ViewEventCloseScreen
import velichko.semyon.demo.ui.event.item.ViewEventShowDialog
import velichko.semyon.demo.ui.event.item.ViewEventShowToast
import velichko.semyon.demo.ui.extensions.showAlertDialog
import velichko.semyon.demo.ui.view_model.BaseViewModel
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    @Inject
    open lateinit var vm: Lazy<BaseViewModel>

    private lateinit var binding: ViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        inject()
        lifecycle.addObserver(vm.get())
        subscribeViewModelEvents()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.setVariable(getVmId(), vm.get())
    }

    abstract fun inject()

    abstract fun getLayout(): Int

    abstract fun getVmId(): Int

    private fun subscribeViewModelEvents() {

        vm.get().event.observe(this, Observer {

            if (it == null || activity == null) return@Observer
            parseEventItem(it)
        })
    }

    protected open fun parseEventItem(item: ViewEvent) {

        when (item) {
            is ViewEventCloseScreen -> activity?.finish()
            is ViewEventShowDialog -> context?.showAlertDialog(item.model)
            is ViewEventShowToast -> Toast.makeText(context, item.message, Toast.LENGTH_SHORT).show()
        }
    }
}