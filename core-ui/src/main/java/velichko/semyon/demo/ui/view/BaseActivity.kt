package velichko.semyon.demo.ui.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import velichko.semyon.demo.ui.R

abstract class BaseActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_container)
        setFragment()
    }

    private fun setFragment() {

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_fragment, getFragment())
                .commit()
    }

    abstract fun getFragment(): Fragment
}