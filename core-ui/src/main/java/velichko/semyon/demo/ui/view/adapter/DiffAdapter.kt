package velichko.semyon.demo.ui.view.adapter

interface DiffAdapter<T> {

    fun updateItems(items: List<T>)

    fun add(item: T)
    fun addAll(items: List<T>)

    fun addAtPosition(pos: Int, item: T)
    fun addAllAtPosition(pos: Int, items: ArrayList<T>)

    fun clearAll()
}