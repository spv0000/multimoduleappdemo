package velichko.semyon.demo.ui.binding

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide

@BindingAdapter("url")
fun loadPictureByUrl(imageView: ImageView, url: String?) {

    Glide.with(imageView.context)
        .load(url)
        .into(imageView)
}