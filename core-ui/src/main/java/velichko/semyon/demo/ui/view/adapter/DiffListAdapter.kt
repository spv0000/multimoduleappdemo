package velichko.semyon.demo.ui.view.adapter

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class DiffListAdapter<T> : RecyclerView.Adapter<BaseViewHolder>(), DiffAdapter<T> {

    private var mItems = ArrayList<T>()

    private val mDiffCallback by lazy { DiffCallbackImpl() }

    protected abstract fun getListItemLayoutId(type: Int = 0): Int

    protected abstract fun fillHolderViews(
        holder: BaseViewHolder?,
        item: T,
        viewType: Int,
        payloads: MutableList<Any>? = null
    )

    protected open fun getChangePayload(oldItem: T, newItem: T): String? = null

    abstract fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    abstract fun areContentsTheSame(oldItem: T, newItem: T): Boolean

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val viewDataBinding: ViewDataBinding =
            DataBindingUtil.inflate(inflater!!, getListItemLayoutId(viewType), parent, false)

        return BaseViewHolder(viewDataBinding, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {

        if (isValidPosition(position).not()) return

        val item = mItems[position]

        fillHolderViews(holder, item, getItemViewType(position))
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int, payloads: MutableList<Any>) {

        if (isValidPosition(position).not()) return

        val item = mItems[position]

        fillHolderViews(holder, item, getItemViewType(position), payloads)
    }

    override fun getItemCount() = mItems.size

    private fun isValidPosition(position: Int) = mItems.isNotEmpty() && mItems.size > position

    protected fun getItem(position: Int): T? {

        if (isValidPosition(position).not()) return null

        return mItems[position]
    }

    fun getItemAtPosition(position: Int): T? {
        if (isValidPosition(position).not()) return null

        return mItems[position]
    }

    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        val pos = mItems.size - position
        return super.getItemId(pos)
    }

    fun getItems(): ArrayList<T> = mItems

    override fun add(item: T) {

        val newItems = ArrayList(mItems)

        newItems.add(item)

        updateItems(newItems)
    }

    override fun addAll(items: List<T>) {

        val newItems = ArrayList(mItems)

        newItems.addAll(items)

        updateItems(newItems)
    }

    override fun addAtPosition(pos: Int, item: T) {

        val newItems = ArrayList(mItems)

        newItems.add(pos, item)

        updateItems(newItems)
    }

    override fun addAllAtPosition(pos: Int, items: ArrayList<T>) {

        val newItems = ArrayList(mItems)

        newItems.addAll(pos, items)

        updateItems(newItems)
    }

    fun removeItemAtPosition(position: Int) {
        if (isValidPosition(position).not()) return

        val newItems = ArrayList(mItems)

        newItems.removeAt(position)

        updateItems(newItems)
    }

    fun removeByCondition(removeCondition: (T) -> Boolean) {

        val tempItems = ArrayList(mItems.filterNot(removeCondition))

        updateItems(tempItems)
    }

    fun removeItemsAtRange(range: IntRange) {
        if (isValidPosition(range.last).not()) return

        val newItems = ArrayList(mItems)

        for (index in range.last downTo range.first) {

            newItems.removeAt(index)
        }

        updateItems(newItems)
    }

    override fun clearAll() {

        mItems.clear()

        notifyDataSetChanged()
    }

    override fun updateItems(items: List<T>) {

        if (items.isEmpty()) {
            clearAll()
            return
        }

        mDiffCallback.setLists(mItems, items)

        val diffResult = DiffUtil.calculateDiff(mDiffCallback)

        mItems.clear()
        mItems.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    //--
    private inner class DiffCallbackImpl : DiffUtil.Callback() {

        private val mOldItems = ArrayList<T>()
        private val mNewItems = ArrayList<T>()

        fun setLists(
            oldItems: List<T>,
            newItems: List<T>
        ) {

            mOldItems.clear()
            mOldItems.addAll(oldItems)

            mNewItems.clear()
            mNewItems.addAll(newItems)
        }

        override fun getOldListSize(): Int = mOldItems.size

        override fun getNewListSize(): Int = mNewItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

            val oldItem = mOldItems[oldItemPosition]
            val newItem = mNewItems[newItemPosition]

            return this@DiffListAdapter.areItemsTheSame(oldItem, newItem)
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

            val oldItem = mOldItems[oldItemPosition]
            val newItem = mNewItems[newItemPosition]

            return this@DiffListAdapter.areContentsTheSame(oldItem, newItem)
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {

            val oldItem = mOldItems[oldItemPosition]
            val newItem = mNewItems[newItemPosition]

            return this@DiffListAdapter.getChangePayload(oldItem, newItem)
        }
    }
}