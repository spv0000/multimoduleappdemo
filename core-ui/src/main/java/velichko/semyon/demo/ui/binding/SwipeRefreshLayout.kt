package velichko.semyon.demo.ui.binding

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout

@BindingAdapter("isRefreshing")
fun setRefreshingMode(swipe: SwipeRefreshLayout, isRefreshing: Boolean) {

    swipe.isRefreshing = isRefreshing
}

@BindingAdapter("listener")
fun setListener(swipe: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener) {

    swipe.setOnRefreshListener(listener)
}