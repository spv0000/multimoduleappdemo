package velichko.semyon.demo.ui.binding

import android.databinding.BindingAdapter
import android.support.v7.widget.SearchView
import android.view.View
import velichko.semyon.demo.ui.extensions.hideSoftKeyboard

@BindingAdapter("listener")
fun setListener(searchView: SearchView, listener: SearchView.OnQueryTextListener) {

    searchView.setOnQueryTextListener(listener)
}

@BindingAdapter("enable")
fun setEnabled(searchView: SearchView, enable: Boolean) {

    when (enable) {
        true -> searchView.onActionViewExpanded()
        else -> {
            searchView.onActionViewCollapsed()
            searchView.hideSoftKeyboard()
        }
    }
}

@BindingAdapter("query")
fun setQuery(searchView: SearchView, query: String) {

    searchView.setQuery(query, false)
}

@BindingAdapter("focusListener")
fun setFocusListener(searchView: SearchView, listener: View.OnFocusChangeListener) {

    searchView.setOnQueryTextFocusChangeListener(listener)
}