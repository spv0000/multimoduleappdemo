package velichko.semyon.demo.ui.extensions

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager

@TargetApi(Build.VERSION_CODES.CUPCAKE)
fun View.hideSoftKeyboard() {

    val manager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    manager.hideSoftInputFromWindow(this.windowToken, 0)
}