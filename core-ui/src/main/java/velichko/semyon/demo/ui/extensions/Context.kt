package velichko.semyon.demo.ui.extensions

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import velichko.semyon.demo.ui.view.model.DialogModel


fun Context.showAlertDialog(

        model: DialogModel
) {

    val builder = AlertDialog.Builder(this)

    builder.setTitle(model.title)
    builder.setMessage(model.text)
    builder.setPositiveButton(model.positiveText) { dialog, _ -> invokePositiveAction(model.positiveAction, dialog) }
    builder.setNegativeButton(model.negativeText) { dialog, _ -> invokeNegativeAction(model.negativeAction!!, dialog) }
    builder.setOnCancelListener { model.onCancelAction?.invoke() }
    builder.create().show()
}

private fun invokePositiveAction(positiveAction: (() -> Unit)?, dialog: DialogInterface) {
    positiveAction?.invoke()
    dialog.cancel()
}

private fun invokeNegativeAction(negativeAction: () -> Unit, dialog: DialogInterface) {
    negativeAction.invoke()
    dialog.cancel()
}
