package velichko.semyon.demo.ui

import android.arch.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import velichko.semyon.demo.ui.view.BaseFragment
import velichko.semyon.demo.ui.view_model.BaseViewModel
import velichko.semyon.demo.ui.view_model.BaseViewModelFactory

@Module
abstract class BaseModule<T : BaseViewModel> {

    abstract val baseFragment: BaseFragment

    @Provides
    fun provides(): BaseViewModel {

        val viewModel = getViewModel()

        val factory = object : BaseViewModelFactory<T>() {
            override fun getViewModel() = viewModel
        }

        return ViewModelProviders.of(baseFragment, factory).get(viewModel::class.java)
    }

    abstract fun getViewModel(): T
}