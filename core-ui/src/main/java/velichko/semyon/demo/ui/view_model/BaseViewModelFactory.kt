package velichko.semyon.demo.ui.view_model

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

abstract class BaseViewModelFactory<T : ViewModel> : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    final override fun <T : ViewModel> create(modelClass: Class<T>): T {

        val viewModel = getViewModel()

        return when (modelClass.isAssignableFrom(viewModel::class.java)) {

            true -> viewModel as T
            else -> throw IllegalArgumentException("incorrect viewModel for factory class = $modelClass")
        }
    }

    abstract fun getViewModel(): T
}