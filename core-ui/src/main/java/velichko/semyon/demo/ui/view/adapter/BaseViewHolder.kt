package velichko.semyon.demo.ui.view.adapter

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

open class BaseViewHolder(

    val binding: ViewDataBinding,
    val viewType: Int = -1

) : RecyclerView.ViewHolder(binding.root)