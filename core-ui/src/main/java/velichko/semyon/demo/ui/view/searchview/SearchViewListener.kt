package velichko.semyon.demo.ui.view.searchview

import android.support.v7.widget.SearchView

class SearchViewListener(

    private val onTextSubmit: (query: String) -> Unit,
    private val onTextChanged: (query: String) -> Unit

) : SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String): Boolean {
        onTextSubmit.invoke(query)
        return true
    }

    override fun onQueryTextChange(query: String): Boolean {
        onTextChanged.invoke(query)
        return true
    }
}