package velichko.semyon.demo.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet

class SquareImageView(context: Context, attrs: AttributeSet) : AppCompatImageView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val minSide = Math.min(widthMeasureSpec, heightMeasureSpec)
        super.onMeasure(minSide, minSide)
    }
}