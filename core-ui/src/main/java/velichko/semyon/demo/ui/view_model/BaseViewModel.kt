package velichko.semyon.demo.ui.view_model

import android.arch.lifecycle.*
import velichko.semyon.demo.ui.event.Event
import velichko.semyon.demo.ui.event.item.ViewEvent

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun init(owner: LifecycleOwner) {

        initData(owner)
    }

    open fun initData(owner: LifecycleOwner) {}

    val event = Event<ViewEvent>()

    fun postViewEvent(eventItem: ViewEvent) {
        event.value = eventItem
    }
}