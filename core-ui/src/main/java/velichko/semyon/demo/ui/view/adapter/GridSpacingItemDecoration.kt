package velichko.semyon.demo.ui.view.adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View


internal class GridSpacingItemDecoration(

    private val space: Int

) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        outRect.set(Rect(space, space, space, space))
    }
}