package velichko.semyon.demo.ui.view.model

class DialogModel(

        val title: String? = null,
        val text: String? = null,
        val positiveText: String? = null,
        val negativeText: String? = null,
        val positiveAction: (() -> Unit)? = null,
        val negativeAction: (() -> Unit)? = null,
        val onCancelAction: (() -> Unit)? = null
)