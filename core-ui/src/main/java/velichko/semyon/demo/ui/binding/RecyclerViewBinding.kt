package velichko.semyon.demo.ui.binding

import android.databinding.BindingAdapter
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import velichko.semyon.demo.ui.view.adapter.DiffListAdapter
import velichko.semyon.demo.ui.view.adapter.GridSpacingItemDecoration

@BindingAdapter("diffAdapter")
fun <T> setRecyclerViewDiffAdapter(rv: RecyclerView, adapter: DiffListAdapter<T>) {

    rv.adapter = adapter
}

@BindingAdapter("adapter")
fun <T: RecyclerView.ViewHolder> setRecyclerViewAdapter(rv: RecyclerView, adapter: RecyclerView.Adapter<T>) {

    rv.adapter = adapter
}

@BindingAdapter("fixedSize")
fun setRecyclerViewFixedSize(rv: RecyclerView, isFixedSize: Boolean) {

    rv.setHasFixedSize(isFixedSize)
}

@BindingAdapter("gridLayoutManagerColumnsCount")
fun setGridLayoutManagerColumnsCount(rv: RecyclerView, columns: Int) {

    rv.layoutManager = GridLayoutManager(rv.context, columns)
}

@BindingAdapter("decorationSpace")
fun setDecoration(rv: RecyclerView, space: Int) {

    rv.addItemDecoration(GridSpacingItemDecoration(space))
}
