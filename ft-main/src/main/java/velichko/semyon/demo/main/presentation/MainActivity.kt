package velichko.semyon.demo.main.presentation

import android.annotation.SuppressLint
import velichko.semyon.demo.ui.view.BaseActivity

@SuppressLint("Registered")
internal class MainActivity : BaseActivity() {

    override fun getFragment() = MainFragment()
}