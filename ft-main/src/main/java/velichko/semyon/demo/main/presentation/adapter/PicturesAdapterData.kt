package velichko.semyon.demo.main.presentation.adapter

import velichko.semyon.demo.core.di.scopes.info.ScreenInfo

internal class PicturesAdapterData(

    private val screenInfo: ScreenInfo

) {

    val columns: Int
        get() {
            return when (screenInfo.isVerticalOrientation()) {
                true -> 3
                else -> 5
            }
        }

    val space: Int = 8

    fun getCardSizePixels(): Int {

        val spaces = columns + 1
        val screenSize = screenInfo.getScreenSize()
        val width = screenSize.x

        return (width - (space * spaces)) / columns
    }
}