package velichko.semyon.demo.main.data.pictures

import io.reactivex.Single

internal interface PicturesRepository {

    fun getPictures(searchQuery: String): Single<SearchPicturesResponse>
}