package velichko.semyon.demo.main.domain.pictures

import io.reactivex.SingleObserver

internal interface GetPicturesCase {

    fun getPictures(searchQuery: String, observer: SingleObserver<ArrayList<PictureModel>>)

}