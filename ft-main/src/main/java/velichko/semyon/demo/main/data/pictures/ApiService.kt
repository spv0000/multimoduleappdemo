package velichko.semyon.demo.main.data.pictures

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

internal interface ApiService {

    @GET("services/rest/")
    fun search(

        @Query("text") query: String,
        @Query("per_page") count: Int = 100,
        @Query("page") pageNumber: Int = 0,
        @Query("method") method: String = "flickr.photos.search",
        @Query("api_key") apiKey: String = "ceae9648dd84059b78277d0be5c13ebf",
        @Query("format") format: String = "json",
        @Query("nojsoncallback") noJsonCallback: String = "1"

    ): Single<SearchPicturesResponse>
}