package velichko.semyon.demo.main.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import velichko.semyon.demo.main.R

internal class SuggestionsAdapter(

    private val action: (suggestion: String) -> Unit

) : RecyclerView.Adapter<SuggestionsViewHolder>() {

    var items = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): SuggestionsViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.li_suggestion, parent, false)

        return SuggestionsViewHolder(view)
    }

    override fun onBindViewHolder(holder: SuggestionsViewHolder, position: Int) {
        holder.suggestionView.text = items[position]
        holder.suggestionView.setOnClickListener { action.invoke(items[position]) }
    }

    override fun getItemCount() = items.size
}

internal class SuggestionsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val suggestionView = view.findViewById<TextView>(R.id.li_suggestion_text)!!
}
