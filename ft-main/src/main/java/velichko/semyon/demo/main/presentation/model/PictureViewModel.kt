package velichko.semyon.demo.main.presentation.model

internal data class PictureViewModel(

    val url: String,
    val action: (url: String) -> Unit
)