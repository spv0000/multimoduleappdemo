package velichko.semyon.demo.main.presentation.di

import dagger.Component
import velichko.semyon.demo.core.di.scopes.di.AppComponent
import velichko.semyon.demo.main.presentation.MainFragment

@MainScreenScope
@Component(

    dependencies = [AppComponent::class],
    modules = [MainModule::class]

)
interface MainComponent {

    fun injectInto(fragment: MainFragment)
}