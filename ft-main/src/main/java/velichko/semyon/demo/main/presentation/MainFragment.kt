package velichko.semyon.demo.main.presentation

import velichko.semyon.demo.core.di.scopes.di.AppComponentProvider
import velichko.semyon.demo.main.BR
import velichko.semyon.demo.main.R
import velichko.semyon.demo.main.presentation.di.DaggerMainComponent
import velichko.semyon.demo.main.presentation.di.MainModule
import velichko.semyon.demo.ui.view.BaseFragment

class MainFragment : BaseFragment() {

    override fun inject() {

        DaggerMainComponent
            .builder()
            .appComponent(AppComponentProvider.component)
            .mainModule(MainModule(this))
            .build()
            .injectInto(this)
    }

    override fun getLayout() = R.layout.fragment_main

    override fun getVmId() = BR.vmMain
}