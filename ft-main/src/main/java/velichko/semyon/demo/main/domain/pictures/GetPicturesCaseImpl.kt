package velichko.semyon.demo.main.domain.pictures

import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import velichko.semyon.demo.main.data.pictures.PicturesRepository

internal class GetPicturesCaseImpl(

    private val repository: PicturesRepository

) : GetPicturesCase {

    private val mapper = PictureMapper()

    override fun getPictures(searchQuery: String, observer: SingleObserver<ArrayList<PictureModel>>) {

        repository
            .getPictures(searchQuery)
            .map { it.photos.photo.mapTo(ArrayList()) { mapper.apply(it) } }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(observer)
    }
}