package velichko.semyon.demo.main.data.pictures

import io.reactivex.Single
import retrofit2.Retrofit


internal class PicturesRepositoryImpl(private val retrofit: Retrofit) :
    PicturesRepository {

    override fun getPictures(searchQuery: String): Single<SearchPicturesResponse> {

        return retrofit.create(ApiService::class.java).search(query = searchQuery)
    }
}