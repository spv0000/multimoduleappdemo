package velichko.semyon.demo.main.presentation

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import velichko.semyon.demo.core.di.scopes.info.ScreenInfo
import velichko.semyon.demo.core.di.scopes.observer.ConciseObserver
import velichko.semyon.demo.core.di.scopes.router.Router
import velichko.semyon.demo.main.domain.pictures.GetPicturesCase
import velichko.semyon.demo.main.domain.pictures.PictureModel
import velichko.semyon.demo.main.domain.suggestions.SuggestionsCase
import velichko.semyon.demo.main.presentation.adapter.PicturesAdapter
import velichko.semyon.demo.main.presentation.adapter.PicturesAdapterData
import velichko.semyon.demo.main.presentation.adapter.SuggestionsAdapter
import velichko.semyon.demo.main.presentation.model.PictureViewModelMapper
import velichko.semyon.demo.ui.event.item.ViewEventShowToast
import velichko.semyon.demo.ui.view.searchview.SearchViewListener
import velichko.semyon.demo.ui.view_model.BaseViewModel


internal class MainViewModel(

    private val router: Router,
    private val getPicturesCase: GetPicturesCase,
    private val suggestionCase: SuggestionsCase,
    screenInfo: ScreenInfo

) : BaseViewModel() {

    val isFocused = ObservableBoolean(true)
    val isSearching = ObservableBoolean(false)
    val isSuggestionsVisible = ObservableBoolean(false)
    val searchQuery = ObservableField<String>("")

    val picturesAdapterData = PicturesAdapterData(screenInfo = screenInfo)
    val suggestionsAdapter = SuggestionsAdapter(::searchPicture)
    val picturesAdapter = PicturesAdapter(picturesAdapterData.getCardSizePixels())
    val searchQueryListener = SearchViewListener(::searchPicture, ::filterSuggestions)
    val swipeListener = SwipeRefreshLayout.OnRefreshListener { searchPicture(searchQuery.get()!!) }
    val focusListener = View.OnFocusChangeListener { _, hasFocus -> showSuggestions(hasFocus) }

    private val mapper by lazy { PictureViewModelMapper(::showDetailsScreen) }
    private var items = ArrayList<String>()

    private fun searchPicture(query: String) {
        if (query.isBlank()) return

        isFocused.set(false)
        isSearching.set(true)
        searchQuery.set(query)
        isSuggestionsVisible.set(false)
        suggestionCase.addSuggestion(query)

        getPicturesCase.getPictures(query, ConciseObserver(::onSuccessAction, ::onErrorAction))
    }

    private fun onSuccessAction(list: ArrayList<PictureModel>) {
        picturesAdapter.updateItems(list.mapTo(ArrayList()) { mapper.apply(it) })
        isSearching.set(false)
    }

    private fun onErrorAction() {
        isSearching.set(false)
        postViewEvent(ViewEventShowToast("something went wrong"))
    }

    private fun showDetailsScreen(url: String) {
        router.showDetails(url)
    }

    private fun showSuggestions(hasFocus: Boolean) {

        isSuggestionsVisible.set(hasFocus)
        isFocused.set(hasFocus)

        if (hasFocus.not()) return
        items = ArrayList(suggestionCase.getSuggestions())
        suggestionsAdapter.items = items
        suggestionsAdapter.notifyDataSetChanged()
    }

    private fun filterSuggestions(query: String) {

        suggestionsAdapter.items = ArrayList(items.filter { it.contains(query) })
        suggestionsAdapter.notifyDataSetChanged()
    }
}