package velichko.semyon.demo.main.domain.suggestions

import velichko.semyon.demo.main.data.suggestions.SuggestionsRepositoty

internal class SuggestionsCaseImpl(

    private val repository: SuggestionsRepositoty

) : SuggestionsCase {

    override fun addSuggestion(suggestion: String) {

        repository.addSuggestion(suggestion)
    }

    override fun getSuggestions() = repository.getSuggestions()
}