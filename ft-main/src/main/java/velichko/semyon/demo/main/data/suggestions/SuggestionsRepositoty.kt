package velichko.semyon.demo.main.data.suggestions

internal interface SuggestionsRepositoty {

    fun addSuggestion(suggestion: String)

    fun getSuggestions(): Set<String>
}