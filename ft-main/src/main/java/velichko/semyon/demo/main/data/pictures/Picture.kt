package velichko.semyon.demo.main.data.pictures

internal data class Picture(

    val id: String?,
    val title: String?,
    val secret: String?,
    val server: String?,
    val farm: String?
)