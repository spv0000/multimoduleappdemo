package velichko.semyon.demo.main.domain.suggestions

internal interface SuggestionsCase {

    fun addSuggestion(suggestion: String)

    fun getSuggestions(): Set<String>
}