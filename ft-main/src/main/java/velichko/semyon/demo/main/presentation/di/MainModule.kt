package velichko.semyon.demo.main.presentation.di

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import velichko.semyon.demo.core.di.scopes.info.ScreenInfo
import velichko.semyon.demo.core.di.scopes.router.Router
import velichko.semyon.demo.main.data.pictures.PicturesRepository
import velichko.semyon.demo.main.data.pictures.PicturesRepositoryImpl
import velichko.semyon.demo.main.data.suggestions.SuggestionsRepositoty
import velichko.semyon.demo.main.data.suggestions.SuggestionsRepositotyImpl
import velichko.semyon.demo.main.domain.pictures.GetPicturesCase
import velichko.semyon.demo.main.domain.pictures.GetPicturesCaseImpl
import velichko.semyon.demo.main.domain.suggestions.SuggestionsCase
import velichko.semyon.demo.main.domain.suggestions.SuggestionsCaseImpl
import velichko.semyon.demo.main.presentation.MainViewModel
import velichko.semyon.demo.ui.view.BaseFragment
import velichko.semyon.demo.ui.view_model.BaseViewModel
import velichko.semyon.demo.ui.view_model.BaseViewModelFactory

@Module
class MainModule(

    private val baseFragment: BaseFragment

) {

    @Provides
    @MainScreenScope
    fun provides(router: Router, screenInfo: ScreenInfo, retrofit: Retrofit, context: Context): BaseViewModel {

        val picturesRepository: PicturesRepository = PicturesRepositoryImpl(retrofit)
        val getPicturesCase: GetPicturesCase = GetPicturesCaseImpl(picturesRepository)

        val suggestionsRepository: SuggestionsRepositoty = SuggestionsRepositotyImpl(context)
        val suggestionsCase: SuggestionsCase = SuggestionsCaseImpl(suggestionsRepository)

        val factory = object : BaseViewModelFactory<MainViewModel>() {
            override fun getViewModel() = MainViewModel(router, getPicturesCase, suggestionsCase, screenInfo)
        }

        return ViewModelProviders.of(baseFragment.activity!!, factory).get(MainViewModel::class.java)
    }
}