package velichko.semyon.demo.main.data.suggestions

import android.content.Context
import java.util.*

private const val KEY_SUGGESTIONS = "key_suggestions"

class SuggestionsRepositotyImpl(context: Context) : SuggestionsRepositoty {

    private val prefs by lazy {
        context.getSharedPreferences(this::class.java.simpleName, Context.MODE_PRIVATE)
    }

    override fun addSuggestion(suggestion: String) {

        val suggestions = getSuggestionsFromPrefs()
        suggestions.add(suggestion)
        prefs.edit().putStringSet(KEY_SUGGESTIONS, suggestions).apply()
    }

    private fun getSuggestionsFromPrefs() = TreeSet(prefs.getStringSet(KEY_SUGGESTIONS, TreeSet<String>()))

    override fun getSuggestions(): Set<String> = getSuggestionsFromPrefs()
}