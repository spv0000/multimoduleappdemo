package velichko.semyon.demo.main.data.pictures

internal data class SearchPicturesResponse (

    val photos: PictureResponse
)

internal data class PictureResponse (

    val photo: List<Picture>
)