package velichko.semyon.demo.main.domain.pictures

import io.reactivex.functions.Function
import velichko.semyon.demo.main.data.pictures.Picture

internal class PictureMapper : Function<Picture, PictureModel> {

    override fun apply(pic: Picture) = PictureModel(getUrl(pic))

    private fun getUrl(pic: Picture): String {
        return "https://farm${pic.farm}.staticflickr.com/${pic.server}/${pic.id}_${pic.secret}.jpg"
    }
}