package velichko.semyon.demo.main.presentation.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import velichko.semyon.demo.main.BR
import velichko.semyon.demo.main.R
import velichko.semyon.demo.main.presentation.model.PictureViewModel
import velichko.semyon.demo.ui.view.adapter.BaseViewHolder
import velichko.semyon.demo.ui.view.adapter.DiffListAdapter

internal class PicturesAdapter(

    private val cardSize: Int

) : DiffListAdapter<PictureViewModel>() {

    override fun getListItemLayoutId(type: Int) = R.layout.li_picture

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val holder = super.onCreateViewHolder(parent, viewType)

        applyCardSize(holder, cardSize)

        return holder
    }

    private fun applyCardSize(holder: BaseViewHolder, cardSize: Int) {
        val card = holder.itemView.findViewById<View>(R.id.card)
        card.layoutParams = RelativeLayout.LayoutParams(cardSize, cardSize)
    }

    override fun fillHolderViews(holder: BaseViewHolder?, item: PictureViewModel,
                                 viewType: Int, payloads: MutableList<Any>?) {

        val binding = holder?.binding ?: return
        binding.setVariable(BR.vmPicture, item)
    }

    override fun areItemsTheSame(oldItem: PictureViewModel, newItem: PictureViewModel) = oldItem.url == newItem.url

    override fun areContentsTheSame(oldItem: PictureViewModel, newItem: PictureViewModel) = true
}