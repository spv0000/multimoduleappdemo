package velichko.semyon.demo.main.domain.pictures

data class PictureModel(

    val url: String
)