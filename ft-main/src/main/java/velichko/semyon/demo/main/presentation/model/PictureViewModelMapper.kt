package velichko.semyon.demo.main.presentation.model

import io.reactivex.functions.Function
import velichko.semyon.demo.main.domain.pictures.PictureModel

internal class PictureViewModelMapper(val action: (url: String) -> Unit) : Function<PictureModel, PictureViewModel> {

    override fun apply(model: PictureModel) = PictureViewModel(
        url = model.url,
        action = action
    )
}
