##Multi-module demo app

####Stack:

*  MVVM
*  Databinding
*  Architecture components
*  RxJava
*  Retrofit

####Modules links scheme:
![](scheme.png)