package velichko.semyon.demo.details.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DetailsScreenScope