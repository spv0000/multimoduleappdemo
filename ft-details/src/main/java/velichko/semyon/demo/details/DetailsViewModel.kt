package velichko.semyon.demo.details

import velichko.semyon.demo.ui.view_model.BaseViewModel

internal class DetailsViewModel(

    val url: String?

) : BaseViewModel()