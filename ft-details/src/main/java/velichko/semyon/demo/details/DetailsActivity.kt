package velichko.semyon.demo.details

import android.annotation.SuppressLint
import android.os.Bundle
import velichko.semyon.demo.core.di.scopes.navigator.KEY_URL
import velichko.semyon.demo.ui.view.BaseActivity

@SuppressLint("Registered")
class DetailsActivity : BaseActivity() {

    override fun getFragment() = DetailsFragment().apply {
        val bundle = Bundle()
        bundle.putString(KEY_URL, intent.getStringExtra(KEY_URL))
        arguments = bundle
    }
}