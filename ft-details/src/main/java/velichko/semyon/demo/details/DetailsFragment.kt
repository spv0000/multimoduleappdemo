package velichko.semyon.demo.details

import velichko.semyon.demo.core.di.scopes.di.AppComponentProvider
import velichko.semyon.demo.core.di.scopes.navigator.KEY_URL
import velichko.semyon.demo.details.di.DaggerDetailsComponent
import velichko.semyon.demo.details.di.DetailsModule
import velichko.semyon.demo.ui.view.BaseFragment

class DetailsFragment : BaseFragment() {

    override fun inject() {

        DaggerDetailsComponent
            .builder()
            .appComponent(AppComponentProvider.component)
            .detailsModule(DetailsModule(this, arguments?.getString(KEY_URL)))
            .build()
            .injectInto(this)
    }

    override fun getLayout() = R.layout.fragment_details

    override fun getVmId() = BR.vmDetails
}