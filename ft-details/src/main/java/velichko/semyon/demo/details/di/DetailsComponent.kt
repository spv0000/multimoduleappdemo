package velichko.semyon.demo.details.di

import dagger.Component
import velichko.semyon.demo.core.di.scopes.di.AppComponent
import velichko.semyon.demo.details.DetailsFragment

@DetailsScreenScope
@Component(

    dependencies = [AppComponent::class],
    modules = [DetailsModule::class]

)
internal interface DetailsComponent {

    fun injectInto(fragment: DetailsFragment)
}