package velichko.semyon.demo.details.di

import android.arch.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import velichko.semyon.demo.details.DetailsViewModel
import velichko.semyon.demo.ui.view.BaseFragment
import velichko.semyon.demo.ui.view_model.BaseViewModel
import velichko.semyon.demo.ui.view_model.BaseViewModelFactory

@Module
internal class DetailsModule(

    private val baseFragment: BaseFragment,
    private val url: String?

) {

    @Provides
    @DetailsScreenScope
    fun provides(): BaseViewModel {

        val factory = object : BaseViewModelFactory<DetailsViewModel>() {
            override fun getViewModel() = DetailsViewModel(url)
        }

        return ViewModelProviders.of(baseFragment.activity!!, factory).get(DetailsViewModel::class.java)
    }
}